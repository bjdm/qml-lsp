# qml-lsp tool collection

This is the qml-lsp tool collection, a collection of
dev tools for working with QML.

- qml-lsp: LSP implementation for QML (autocompletion, live linting, etc. in editors)
- qml-dap: DAP implementation for QML (allows debugging QML in supporting editors like VSCode)
- qml-dbg: command-line debugger for QML
- qml-lint: command-line linter for QML
- qml-refactor-fairy: bulk refactoring tool for QML
