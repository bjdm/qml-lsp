module qml-lsp

go 1.16

require (
	github.com/alecthomas/participle/v2 v2.0.0-alpha7
	github.com/alecthomas/repr v0.0.0-20210801044451-80ca428c5142 // indirect
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e // indirect
	github.com/google/go-dap v0.6.0
	github.com/iafan/cwalk v0.0.0-20210125030640-586a8832a711 // indirect
	github.com/mattn/go-pointer v0.0.1
	github.com/smacker/go-tree-sitter v0.0.0-20210922091224-7d35f700adf0
	github.com/sourcegraph/jsonrpc2 v0.1.0
	github.com/urfave/cli/v2 v2.3.0 // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
)
